using System;
using DotBuilder.Attributes;

namespace DotBuilder.Statements
{
    public class AttributesFor<T> : Statement<AttributesFor<T>, T> where T : IAttribute
    {
        private readonly string _type;

        internal AttributesFor(string type)
        {
            _type = type;
        }

        public override string Render()
        {
            return $"{_type} {base.Render()}";
        }
    }

    public static class AttributesFor
    {
        [Obsolete]
        public static AttributesFor<INodeAttribute> Node => new AttributesFor<INodeAttribute>("node");
        [Obsolete]
        public static AttributesFor<IEdgeAttribute> Edge => new AttributesFor<IEdgeAttribute>("edge");
    }
}
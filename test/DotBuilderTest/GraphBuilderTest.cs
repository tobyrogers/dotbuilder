﻿using System.IO;
using DotBuilder;
using DotBuilder.Attributes;
using DotBuilder.Statements;
using NUnit.Framework;

namespace DotBuilderTest
{
    [TestFixture]
    public class GraphBuilderTest
    {
        [Test]
        public void Test1()
        {
            var graph = Graph.Directed("agraph")
                .WithGraphAttributesOf(
                    RankDir.TB,
                    Font.Name("Arial"),
                    Font.Size(55))
                .WithNodeAttributesOf(
                    Style.Rounded,
                    Style.Filled,
                    Shape.Diamond,
                    Color.Yellowgreen,
                    FillColor.Green)
                .Containing(
                    Edge.Between("node 1", "node 2")
                        .WithAttributesOf(
                            Color.RGB(0, 200, 200),
                            LabelToolTip.Set("I have a label tool tip"),
                            ToolTip.Set("I have an edge tool tip"),
                            Label.Set("edge label")))
                .Containing(
                    Node.Name("node 1")
                        .WithAttributesOf(Shape.Ellipse,
                            Url.Set("http://www.google.com"),
                            ToolTip.Set("This is a tool tip\nand on a\n\tnew line"),
                            FillColor.Aliceblue,
                            FontColor.Red,
                            Attrib.Set("fillcolor", $"#{226:x2}{240:x2}{217:x2}")),
                    Node.Name("node 2"))
                .Containing(Node.Name("node 3"));

            // to get the raw dot output
             var dot = graph.Render();

            // to render to a file
            var gv = new GraphViz(@"C:\Develop\Tools\graphviz\bin", OutputFormat.Svg);
            using (var stream = new FileStream(@"c:\artifacts\dotbuilder\test.svg", FileMode.Create))
            {
                gv.RenderGraph(graph, stream);
            }
        }
    }
}
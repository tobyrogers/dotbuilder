#DotBuilder

This library provides a fluent interface for generating DOT files that can be rendered by GraphVis

It also provides a utility class that uses the GraphVis binaries to render the DOT file to a stream.

GraphVis binaries can be downloaded from https://graphviz.gitlab.io/_pages/Download/Download_windows.html

Here is a example that renders a simple directed graph to an svg file:


    var graph = Graph.Directed("agraph")
                .WithGraphAttributesOf(
                    RankDir.TB,
                    Font.Name("Arial"),
                    Font.Size(55))
                .WithNodeAttributesOf(
                    Style.Rounded,
                    Style.Filled,
                    Shape.Diamond,
                    Color.Yellowgreen,
                    FillColor.Green)
                .Containing(
                    Edge.Between("node 1", "node 2")
                        .WithAttributesOf(
                            Color.RGB(0, 200, 200),
                            LabelToolTip.Set("I have a label tool tip"),
                            ToolTip.Set("I have an edge tool tip"),
                            Label.Set("edge label")))
                .Containing(
                    Node.Name("node 1")
                        .WithAttributesOf(Shape.Ellipse,
                            Url.Set("http://www.google.com"),
                            ToolTip.Set("This is a tool tip\nand on a\n\tnew line"),
                            FillColor.Aliceblue,
                            FontColor.Red,
                            Attrib.Set("fillcolor", $"#{226:x2}{240:x2}{217:x2}")),
                    Node.Name("node 2"))
                .Containing(Node.Name("node 3"));

            // to get the raw dot output
             var dot = graph.Render();

            // to render to a file stream
            var gv = new GraphViz(@"C:\Develop\Tools\graphviz\bin", OutputFormat.Svg);
            using (var stream = new FileStream(@"c:\artifacts\dotbuilder\test.svg", FileMode.Create))
            {
                gv.RenderGraph(graph, stream);
            }
